<?php

declare(strict_types=1);

require __DIR__ . '/vendor/autoload.php';

use OpenTelemetry\{
    SDK\Common\Configuration\Variables as Env,
    SDK\Common\Configuration\KnownValues as EnvValues,
    SDK\Trace\TracerProviderFactory,
    API\Trace\SpanKind,
    SemConv\ResourceAttributes
};

// use 'open-telemetry/exporter-otlp' as exporter
putenv(Env::OTEL_TRACES_EXPORTER . '=' . EnvValues::VALUE_OTLP);
// endpoint for traces sent over HTTP, alternative is gRPC
putenv(Env::OTEL_EXPORTER_OTLP_TRACES_ENDPOINT . '=https://gateway.tempo.shared.dataddo.com:443/otlp/v1/traces');
// Use protobuf body over HTTP
putenv(Env::OTEL_EXPORTER_OTLP_TRACES_PROTOCOL . '=' . EnvValues::VALUE_HTTP_PROTOBUF);
// Don't send every single trace, send in batches + some triggers/thresholds
putenv(Env::OTEL_PHP_TRACES_PROCESSOR . '=' . EnvValues::VALUE_BATCH);
putenv(Env::OTEL_BSP_SCHEDULE_DELAY . '=5000');
putenv(Env::OTEL_BSP_EXPORT_TIMEOUT . '=30000');
putenv(Env::OTEL_BSP_MAX_QUEUE_SIZE . '=2048');
putenv(Env::OTEL_BSP_MAX_EXPORT_BATCH_SIZE . '=512');

$tracerProvider = (new TracerProviderFactory())->create();
$tracer = $tracerProvider->getTracer(
    name: 'open-telemetry/sdk', // name is understood as tracer library name by Grafana Tempo.
    version: \Composer\InstalledVersions::getVersion('open-telemetry/sdk'),
    attributes: [
        // is being sourced from project name defined by name field in composer.json
        ResourceAttributes::SERVICE_NAME => '',
    ],
);

function Fibonacci(int $number)
{
    global $tracer;
    $span = $tracer
        ->spanBuilder("Fibonacci")
        ->setSpanKind(SpanKind::KIND_INTERNAL)
        ->setAttribute('argument', $number)
        ->startSpan();
    try {
        if ($number == 0)
            $res = 0;
        else if ($number == 1)
            $res = 1;
        else
            $res = (
                Fibonacci($number - 1) +
                Fibonacci($number - 2)
            );
    } finally {
        $span
            ->setAttribute("result", $res)
            ->end();
        return $res;
    }
}

function main(): void
{
    global $tracer;
    $span = $tracer
        ->spanBuilder("main")
        ->setSpanKind(SpanKind::KIND_INTERNAL)
        ->startSpan();
    $scope = $span->activate();
    try {
        $res = Fibonacci(10);
        $span->setAttribute("result", $res);
    } finally {
        $span->end();
        $scope->detach();
        echo 'Trace ID: ' . $span->getContext()->getTraceId() . PHP_EOL;
    }
}

try {
    main();
} finally {
// make sure that the traces are sent.
    $tracerProvider->forceFlush();
    $tracerProvider->shutdown();
}
